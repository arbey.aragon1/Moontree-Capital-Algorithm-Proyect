FROM ubuntu:16.04
MAINTAINER ARBEY arbey.aragon@gmail.com

RUN apt-get update && apt-get install -y \
    wget ca-certificates \
    git curl vim python3-dev python3-pip \
    libfreetype6-dev libpng12-dev libhdf5-dev htop screen

RUN pip3 install --upgrade pip setuptools
RUN pip3 install tensorflow
RUN pip3 install numpy pandas sklearn \
    matplotlib seaborn jupyter pyyaml \
    h5py requests six oandapyV20 rx python-firebase

#RUN pip3 install keras --no-deps

RUN ["mkdir", "notebooks"]
COPY jupyter_notebook_config.py /root/.jupyter/
COPY run_jupyter.sh /

EXPOSE 8888 6006

CMD bash
