# External libs
from rx import Observable, Observer
import random
import numpy as np
import pandas as pd
import datetime as dt
pd.set_option('display.float_format', '{:.5g}'.format)

# Proyect libs
import constants
PATH_DATA = constants.PATH_DATA

def getKey(item):
    return item[0]

def empty2(dateInit, dateEnd):
    index = pd.date_range(dateInit, dateEnd, freq='T')
    series = pd.DataFrame(index=index)
    return series

def empty(dateInit):
    dateEnd = str(pd.to_datetime(dateInit) +
                  dt.timedelta(hours=24) - dt.timedelta(minutes=1))
    return empty2(dateInit, dateEnd)



def streamRandomWalk(mlSeconds=1000, initVal=50, mu=1, sigma=0.5, limitData=500):
    return Observable.interval(mlSeconds) \
        .take(limitData) \
        .map(lambda s: {
            'index': 0,
            'price': 0
        }).start_with({
            'index': 0,
            'price': initVal
        }) \
        .scan(lambda x, y: {
            'index': x['index'] + 1,
            'price': x['price'] + random.choice([-1, 1])*mu + np.random.normal(0, sigma)
        })
        
def streamHistData(fileName='data.csv', minDelta=None, mlSeconds=None):
    fileName = PATH_DATA+'/'+fileName

    def mainStream(observer):
        counter = 0
        df = pd.read_csv(fileName, header=None, sep='\t', decimal='.')
        df.index = pd.to_datetime(df[0])
        df = df.drop(df.columns[[0]], axis=1)
        day = 0
        for dateStartDay, groupD in df.groupby(pd.TimeGrouper('D')):
            if(pd.to_datetime(dateStartDay).dayofweek <= 4):

                ddf = empty2(groupD.head(1).index[0],
                             groupD.tail(1).index[0])

                ddf[1] = groupD[1]
                ddf = ddf.fillna(method='ffill').fillna(method='bfill')

                for index, row in ddf.iterrows():
                    val = {
                        'counter': counter,
                        'day': day,
                        'time': str(index),
                        'price': row[1]
                    }
                    observer.on_next(val)
                    counter = counter + 1

                day = day+1

    print('****************************')
    ys = None

    def deleteCounter(y):
        del y['counter']
        return y

    if(minDelta == None): 
        ys = Observable.create(mainStream)
    else:
        ys = Observable.create(mainStream) \
            .filter(lambda s: s['counter'] % minDelta == 0) \
            .map(lambda s: deleteCounter(s))

    def addProp(x, y):
        #y['id'] = x
        return y

    source = None
    if(mlSeconds == None):
        source = ys
    else:
        xs = Observable.interval(mlSeconds)
        source = xs.zip(ys, lambda x, y: addProp(x, y))
    return source
