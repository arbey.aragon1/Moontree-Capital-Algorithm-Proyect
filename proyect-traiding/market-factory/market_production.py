import sys
sys.path.append('../shared')
import constants

from market_interface import MarketInterface

import json
from oandapyV20 import API
from oandapyV20.exceptions import V20Error
from oandapyV20.endpoints.pricing import PricingStream
from rx import Observable, Observer
import configparser
import datetime
import oandapyV20.endpoints.orders as orders

class MarketProduction(MarketInterface):
    __instance = None
    __api = None
    __accountID = None

    def __init__(self, config):
        if MarketProduction.__instance != None:
            raise Exception("Esta clase no es singleton!")
        else:
            MarketProduction.__instance = self
            MarketProduction.__config = config
            MarketProduction.setup()

    @staticmethod
    def setup():
        config = configparser.ConfigParser()
        config.read(constants.PATH_CREDENTIALS_OANDA)

        MarketProduction.__accountID = config['oanda']['account_id']
        access_token = config['oanda']['api_key']

        MarketProduction.__api = API(
            access_token = access_token, 
            environment="practice")

    @staticmethod
    def getInstance(config):
        if MarketProduction.__instance == None:
            MarketProduction(config)
        return MarketProduction.__instance 
    
    def GET_STREAM(self, data):
        instrument = "WTICO_USD"
        def mainStream(observer):
            
            s = PricingStream(
                accountID = MarketProduction.__accountID, 
                params={
                    "instruments":instrument
                    })
            try:
                for R in MarketProduction.__api.request(s):
                    if(R['type'] == 'PRICE'):
                        bid = float(R['bids'][0]['price'])
                        ask = float(R['asks'][0]['price'])
                        timeStamp = R['time']
                        datetime_object = datetime.datetime.strptime(timeStamp[:-4], '%Y-%m-%dT%H:%M:%S.%f')
                        val = {
                            'time': timeStamp,
                            'time2': timeStamp, 
                            'bid': bid, 
                            'ask': ask,
                            'price': bid,
                            'day': datetime_object.day
                            }
                        observer.on_next(val)

            except V20Error as e:
                print("Error2: {}".format(e))

        xs = Observable.interval(60*1000).map(lambda s: str(s))
        ys = Observable.create(mainStream)

        def addProp(x,y):
            y['id'] = x
            now=datetime.datetime.now()
            y['day'] = now.day
            y['time'] = str(now)
            return y

        source = xs.with_latest_from(ys, lambda x, y: addProp(x,y))
        return source

    def BUY(self, order):
        data = {
            'order':{
                "timeInForce": "FOK",
                "instrument": "WTICO_USD",
                "units": "20",
                "type": "MARKET",
                "positionFill": "DEFAULT"
            }
        }
        r = orders.OrderCreate(
            MarketProduction.__accountID, 
            data=data)
        MarketProduction.__api.request(r)

    def SELL(self, order):
        data = {
            'order':{
                "timeInForce": "FOK",
                "instrument": "WTICO_USD",
                "units": "-20",
                "type": "MARKET",
                "positionFill": "DEFAULT"
            }
        }
        r = orders.OrderCreate(
            MarketProduction.__accountID, 
            data=data)
        MarketProduction.__api.request(r)