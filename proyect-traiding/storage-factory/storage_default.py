from storage_interface import StorageInterface

class StorageDefault(StorageInterface):
    __instance = None

    def __init__(self, config):
        if StorageDefault.__instance != None:
            raise Exception("Esta clase no es singleton!")
        else:
            StorageDefault.__instance = self

    @staticmethod
    def getInstance(config):
        print("default getIns")
        print(config)
        if StorageDefault.__instance == None:
            StorageDefault(config)
        return StorageDefault.__instance 
    
    def DATA_IN_SAVE(self, data):
        pass

    def DATA_OUT_SAVE(self, data):
        pass

    def BUY(self, order):
        pass

    def SELL(self, order):
        pass
    
    def STATUS(self, structData):
        pass
