import sys
sys.path.append('../environments')

from environment import STAGING, PRODUCTION, SIMULATION, ENVIRONMENT
from storage_default import StorageDefault
from storage_production import StorageProduction
from storage_simulation import StorageSimulation

class StorageFactory(object):
    @staticmethod
    def getInstance(config):
        if ENVIRONMENT == STAGING:
            return StorageDefault.getInstance(config)
        elif ENVIRONMENT == PRODUCTION:
            return StorageProduction.getInstance(config)
        elif ENVIRONMENT == SIMULATION:
            return StorageSimulation.getInstance(config)
        else:
            return StorageDefault.getInstance(config)
