import csv

class StorageSimulationSaveFilesCsv(object):
    __config = None
    __writer = None
    def __init__(self, config):
        self.__config = config
        csvfile = open(self.__config['fileDir'], 'w')
        self.__writer = csv.DictWriter(
            csvfile, 
            fieldnames = self.__config['fieldnames'], 
            delimiter = ';'
            )
        self.__writer.writeheader()

    def saveRow(self, data):
        self.__writer.writerow(data)
