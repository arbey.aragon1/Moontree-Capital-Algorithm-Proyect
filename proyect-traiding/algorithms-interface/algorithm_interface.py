import sys
sys.path.append('../shared')
sys.path.append('../storage-factory')
sys.path.append('../market-factory')

from constants import LONG, SHORT, ANY, BUY, SELL
from storage_factory import StorageFactory as st_fa
from market_factory import MarketFactory

class AlgorithmInterface(object):
    __config = None
    __sink = None
    
    __temporalOrder = None

    __dao = None
    __market = None

    def __init__(self, config, sink, configDB = None, marketConfig = None):
        global LONG, SHORT, ANY, BUY, SELL
        self.LONG, self.SHORT, self.ANY, self.BUY, self.SELL = LONG, SHORT, ANY, BUY, SELL
        self.__config = config

        self.__market = MarketFactory.getInstance(marketConfig)

        if(configDB != None):
            self.__dao = st_fa.getInstance(configDB)

        self.__sink = sink \
            .do_action(lambda s: self.__saveDataInput(s)) \
            .buffer_with_count(1, self.__config['interval']) \
            .map(lambda s: s[0]) \
            .map(lambda s: self.__mapInput(s))

    def __saveDataInput(self, s):
        if(self.__dao != None):
            self.__dao.DATA_IN_SAVE(s)

    def __mapInput(self, s):
        self.__temporalOrder = {}
        for key, value in s.items():
            self.__temporalOrder[key] = value
        return s

    def sendOrder(self, configOrder):

        for key, value in configOrder.items():
            self.__temporalOrder[key] = value
        
        if(self.__dao != None):
            
            print('**************************')
            print(configOrder)

            if(configOrder['order'] == self.SELL):
                print('order Sell')
                self.__dao.SELL(self.__temporalOrder)
                self.__market.SELL({})

            elif(configOrder['order'] == self.BUY):
                print('order Buy')
                self.__dao.BUY(self.__temporalOrder)
                self.__market.BUY({})

    def __mapOutput(self, s):
        if(self.__dao != None):
            self.__dao.DATA_OUT_SAVE(s)
        return s

    def algoTraiding(self, sink):
        raise Exception("Funcion algoTraiding() no implementada!")

    def flowConnect(self):
        return self.algoTraiding(self.__sink) \
            .map(lambda s: self.__mapOutput(s))