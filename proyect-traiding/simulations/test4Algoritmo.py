# -*- coding: utf-8 -*-
__author__ = 'Arbey Aragon'

from pymongo import MongoClient
import matplotlib.pyplot as plt
import matplotlib.animation as animation
from matplotlib import style
import numpy as np
from rx import Observable, Observer
from time import sleep
import random
import finance_funcs as ff
import returnCalc as rC
import pandas as pd
style.use('seaborn-whitegrid')


BUY, SELL =  2, 1
ANY, LONG, SHORT = 0, 2, 1

ordByEvent = [SELL,BUY,BUY,BUY,SELL,SELL,BUY] 
means = [20,13,10]

dfGlobal = None
retGlobal = None

def meansCalc(array):
    df = pd.DataFrame(array)
    salida = array[-1]
    salida['m1'] = df['price'].iloc[-means[0]:].mean()
    salida['m2'] = df['price'].iloc[-means[1]:].mean()
    salida['m3'] = df['price'].iloc[-means[2]:].mean()
    return salida

def difMeans(a):
    a['m1_m2']=a['m1']-a['m2']
    a['m2_m3']=a['m2']-a['m3']
    a['m1_m3']=a['m1']-a['m3']
    return a

def difInTime(x):
    b = x[1]
    a = x[0]
    b['m1_m2*'] = np.sign(abs(np.sign(b['m1_m2'])-np.sign(a['m1_m2'])))  
    b['m2_m3*'] = np.sign(abs(np.sign(b['m2_m3'])-np.sign(a['m2_m3'])))
    b['m1_m3*'] = np.sign(abs(np.sign(b['m1_m3'])-np.sign(a['m1_m3'])))
    b['m*'] = np.array([b['m1_m2*'], b['m2_m3*'], b['m1_m3*']])
    return b

def OrdersFunc(a):
    global ordByEvent
    a['order'] = 0 # No haga nada
    zInMeanSim = np.count_nonzero(a['m*'] != 0)

    if(zInMeanSim >= 2):
        a['order'] = ordByEvent[0]
    elif(zInMeanSim == 1):
        if(a['m1_m2*']==1):
            if((a['m1'] >= a['m3']) & (a['m2'] >= a['m3'])):
                a['order'] = ordByEvent[1]
            else:
                a['order'] = ordByEvent[2]

        elif(a['m2_m3*']==1):
            if((a['m3'] >= a['m1']) & (a['m2'] >= a['m1'])):
                a['order'] = ordByEvent[3]
            else:
                a['order'] = ordByEvent[4]
            
        elif(a['m1_m3*']==1):
            if((a['m3'] >= a['m2']) & (a['m1'] >= a['m2'])):
                a['order'] = ordByEvent[5]
            else:
                a['order'] = ordByEvent[6] 
    del a['m*']
    return a
    
def PositionCalc(x,y):
    global LONG, SHORT, ANY, BUY, SELL

    valid = False
    salidaBuff = x['buffer']
    salidaActu = y
    salidaActu['position'] = ANY # Ninguna

    salidaActu['positionChanged'] = False

    if (salidaBuff is not None):
        if (salidaActu is not None):
            valid = True
            salidaActu['position'] = salidaBuff['position']
            if ((BUY == salidaActu['order']) or (SELL == salidaActu['order'])):
                
                posiblePos = ANY 
                if (salidaActu['order'] == BUY):
                    posiblePos = LONG
                elif (salidaActu['order'] == SELL):
                    posiblePos = ANY

                if (posiblePos != salidaBuff['position']):
                    salidaActu['positionChanged'] = True
                    salidaBuff = salidaActu
                    if (SELL == salidaActu['order']):
                        salidaBuff['position'] = ANY
                        salidaActu['position'] = ANY
                    elif(BUY == salidaActu['order']):
                        salidaBuff['position'] = LONG
                        salidaActu['position'] = LONG
                        
    elif (salidaActu is not None): 
        valid = True
        if (BUY == salidaActu['order']):
            salidaActu['positionChanged'] = True
            salidaBuff = salidaActu
            salidaBuff['position'] = LONG
            salidaActu['position'] = LONG

    return {
        'buffer': salidaBuff, 
        'actual': salidaActu,
        'valid': valid
        }

def ReturnCalc(x,y):
    global LONG, SHORT, ANY, BUY, SELL
    valid = False
    salidaBuff = x['buffer']
    salidaActu = y
    salidaActu['return'] = 0 
    returnCalculated = False
        
    if (salidaBuff is not None):
        if (salidaActu is not None):
            valid = True
            if (salidaActu['position'] != salidaBuff['position']):
                if (salidaActu['position'] == LONG):
                    salidaBuff = salidaActu
                else:
                    salidaActu['return'] = np.log(salidaActu['price']/salidaBuff['price'])
                    returnCalculated = True
                    salidaBuff = salidaActu

    elif (salidaActu is not None): 
        valid = True
        if (salidaActu['position'] == LONG):
            salidaBuff = salidaActu

    return {
        'buffer': salidaBuff, 
        'actual': salidaActu,
        'valid': valid, 
        'returnCalculated': returnCalculated
        }

def update(s):
    global dfGlobal
    dfGlobal = s

def returns(s):
    global retGlobal
    retGlobal = s

def orderGenerate(s):
    #global idVal, collectionOrders
    #global LONG, SHORT, ANY, BUY, SELL
    #testOrder = DBOrder(idVal, s['price'], s['position'], s['time'])
    #collectionOrders.insert(testOrder.toDBCollection())
    #if(LONG == s['position']):
    #    oO.orderNewBuy()
    #elif(ANY == s['position']):
    #    oO.orderNewSell()
    #print()
    #print('Order')
    #print(testOrder)
    pass

def savePrice(s):
    #global idVal, collectionPrices
    #testPrice = DBPrice(idVal, s['bid'], s['ask'],s['time'] , s['time2'])
    #collectionPrices.insert(testPrice.toDBCollection())
    #print()
    #print('Price')
    #print(testPrice)
    pass
    

flowPrincipal = ff.getHistToStreamOanda(fileName = 'valorQ.txt', mlSeconds = 10, minDelta = 5) \
    .do_action(lambda s: savePrice(s)) \
    .buffer_with_count(max(means),1) \
    .filter(lambda s: s[0]['day'] == s[-1]['day']) \
    .map(lambda s: meansCalc(s)) \
    .map(lambda t: difMeans(t)) \
    .buffer_with_count(2,1) \
    .filter(lambda s: s[0]['day'] == s[-1]['day']) \
    .map(lambda y: difInTime(y)) \
    .map(lambda t: OrdersFunc(t)) \
    .start_with({
        'buffer': None, 
        'actual': None, 
        'valid': False 
        }) \
    .scan(lambda x,y: PositionCalc(x,y) ) \
    .filter(lambda s: s['valid'] ) \
    .map(lambda s: s['actual'] ) \
    .publish().auto_connect(3) \
    

flowPrincipal.filter(lambda s: s['positionChanged']) \
    .subscribe(lambda s: orderGenerate(s))


flowPrincipal.buffer_with_count(max(means)*2,1) \
    .map(lambda s: pd.DataFrame(s)) \
    .subscribe(lambda s: update(s))


rC.retAcCal(flowPrincipal \
    .start_with({'buffer': None, 'actual': None,'valid': False, 'returnCalculated': False}) \
    .scan(lambda x,y: ReturnCalc(x,y) ) \
    .filter(lambda s: s['valid'] ) \
    .filter(lambda s: s['returnCalculated'] ) \
    .map(lambda s: s['actual']['return'])) \
.subscribe(lambda s: returns(s))

fig = plt.figure()

ax1 = plt.subplot2grid((3,3), (0,0), colspan=2)
ax2 = plt.subplot2grid((3,3), (1,0), colspan=2, sharex=ax1, sharey=ax1)
ax3 = plt.subplot2grid((3,3), (2,0), colspan=2, sharex=ax1)
ax4 = plt.subplot2grid((3,3), (0,2), colspan=1)
ax5 = plt.subplot2grid((3,3), (1,2), colspan=1)

def animate(i):
    global dfGlobal, BUY, SELL, ANY, retGlobal
    ax1.clear()
    ax2.clear()
    ax3.clear()
    ax4.clear()
    ax5.clear()
    
    if retGlobal is not None:
        retGTem = retGlobal.copy()
        retGlobalTem, retAcGlobalTem = retGTem['returnList'], retGTem['accumulatedList']
        mapRets = (retGlobalTem != 0)
        retNormal = retGlobalTem[mapRets]*100
        retAcNormal = retAcGlobalTem[mapRets]*100 
        
        ax4.scatter(retNormal.index, retNormal)
        ax5.scatter(retAcNormal.index, retAcNormal)

        ax5.grid(b=True, which='major')
        
        retAccumulated = retNormal.sum()
        
        y = retAccumulated
        x1,x2,y1,y2 = ax5.axis()
        
        if (y >= 0):
            ax5.plot([x1,x2],[y, y], color ='g', linewidth=1.5, linestyle="--")
            ax5.set_title('RetornoAc: '+str(y), fontsize=10, color='g')
        else:
            ax5.plot([x1,x2],[y, y], color ='r', linewidth=1.5, linestyle="--")
            ax5.set_title('RetornoAc: '+str(y), fontsize=10, color='r')

        x1,x2,y1,y2 = ax4.axis()
        y = retNormal.iloc[-1]
        if (y >= 0):
            ax4.plot([x1,x2],[y, y], color ='g', linewidth=1.5, linestyle="--")
            ax4.set_title('Retorno: '+str(y), fontsize=10, color='g')
        else:
            ax4.plot([x1,x2],[y, y], color ='r', linewidth=1.5, linestyle="--")
            ax4.set_title('Retorno: '+str(y), fontsize=10, color='r')
        
    if dfGlobal is not None:
        dfGlobalTem = dfGlobal.copy()
        Orders = dfGlobalTem[dfGlobalTem['order'] != ANY]
        for index, row in Orders.iterrows():
            ysMax = max([row['m1'],row['m2'],row['m3']])
            ysMin = min([row['m1'],row['m2'],row['m3']])
            o = row['order']
            x = index
            ax2.annotate(' '+str(o), xy=(x, ysMax), xytext=(x, ysMax+0.01), arrowprops=dict(facecolor='red', shrink=0.05))
            ax2.plot([x,x],[ysMin,ysMax], color ='blue', linewidth=1.5, linestyle="--")

        ax1.plot(dfGlobalTem.index,dfGlobalTem['price'])
        
        ax2.plot(dfGlobalTem.index,dfGlobalTem['m1'])
        ax2.plot(dfGlobalTem.index,dfGlobalTem['m2'])
        ax2.plot(dfGlobalTem.index,dfGlobalTem['m3'])

        ax3.plot(dfGlobalTem.index,dfGlobalTem['position'])

ani = animation.FuncAnimation(fig, animate, interval=300)
plt.show()