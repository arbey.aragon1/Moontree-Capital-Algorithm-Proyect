# External libs
import sys
import matplotlib.pyplot as plt
import matplotlib.animation as animation
from matplotlib import style
import numpy as np
from rx import Observable, Observer
from time import sleep
import random
import pandas as pd

# Paths
sys.path.append('../shared')
sys.path.append('../algorithms')

# Proyect libs
from rowmoving_averages import rowmoving_averages
import constants
import history_data_service as serviceData
style.use('seaborn-whitegrid')

BUY, SELL = constants.BUY, constants.SELL
ANY, LONG, SHORT = constants.ANY, constants.LONG, constants.SHORT

dfGlobal = None
retGlobal = None

flow = serviceData.getHistData(fileName='data.csv', mlSeconds=10, minDelta=5)

varObjFlow = rowmoving_averages(
    [20, 13, 10], [SELL, BUY, BUY, BUY, SELL, SELL, BUY])

flows = varObjFlow.runFlowPrincipal(flow)


def update(s):
    global dfGlobal
    dfGlobal = s


def returns(s):
    global retGlobal
    retGlobal = s


flows['flowData'].subscribe(lambda s: update(s))
flows['flowRets'].subscribe(lambda s: returns(s))


fig = plt.figure()

ax1 = plt.subplot2grid((3, 3), (0, 0), colspan=2)
ax2 = plt.subplot2grid((3, 3), (1, 0), colspan=2, sharex=ax1, sharey=ax1)
ax3 = plt.subplot2grid((3, 3), (2, 0), colspan=2, sharex=ax1)
ax4 = plt.subplot2grid((3, 3), (0, 2), colspan=1)
ax5 = plt.subplot2grid((3, 3), (1, 2), colspan=1)
ax6 = plt.subplot2grid((3, 3), (2, 2), colspan=1)


def animate(i):
    global dfGlobal, BUY, SELL, ANY, retGlobal
    ax1.clear()
    ax2.clear()
    ax3.clear()
    ax4.clear()
    ax5.clear()
    ax6.clear()

    if retGlobal is not None:
        retGTem = retGlobal.copy()
        retGlobalTem, retAcGlobalTem = retGTem['returnList'], retGTem['accumulatedList']
        mapRets = (retGlobalTem != 0)
        retNormal = retGlobalTem[mapRets]*100
        retAcNormal = retAcGlobalTem[mapRets]*100

        ax4.scatter(retNormal.index, retNormal)
        ax5.scatter(retAcNormal.index, retAcNormal)

        ax5.grid(b=True, which='major')

        retAccumulated = retNormal.sum()

        y = retAccumulated
        x1, x2, y1, y2 = ax5.axis()

        if (y >= 0):
            ax5.plot([x1, x2], [y, y], color='g',
                     linewidth=1.5, linestyle="--")
            ax5.set_title('RetornoAc: '+str(y), fontsize=10, color='g')
        else:
            ax5.plot([x1, x2], [y, y], color='r',
                     linewidth=1.5, linestyle="--")
            ax5.set_title('RetornoAc: '+str(y), fontsize=10, color='r')

        x1, x2, y1, y2 = ax4.axis()
        y = retNormal.iloc[-1]
        if (y >= 0):
            ax4.plot([x1, x2], [y, y], color='g',
                     linewidth=1.5, linestyle="--")
            ax4.set_title('Retorno: '+str(y), fontsize=10, color='g')
        else:
            ax4.plot([x1, x2], [y, y], color='r',
                     linewidth=1.5, linestyle="--")
            ax4.set_title('Retorno: '+str(y), fontsize=10, color='r')

    if dfGlobal is not None:
        dfGlobalTem = dfGlobal.copy()
        Orders = dfGlobalTem[dfGlobalTem['order'] != ANY]
        for index, row in Orders.iterrows():
            ysMax = max([row['m1'], row['m2'], row['m3']])
            ysMin = min([row['m1'], row['m2'], row['m3']])
            o = row['order']
            x = index
            ax2.annotate(' '+str(o), xy=(x, ysMax), xytext=(x, ysMax+0.01),
                         arrowprops=dict(facecolor='red', shrink=0.05))
            ax2.plot([x, x], [ysMin, ysMax], color='blue',
                     linewidth=1.5, linestyle="--")

        ax1.plot(dfGlobalTem.index, dfGlobalTem['price'])

        ax2.plot(dfGlobalTem.index, dfGlobalTem['m1'])
        ax2.plot(dfGlobalTem.index, dfGlobalTem['m2'])
        ax2.plot(dfGlobalTem.index, dfGlobalTem['m3'])

        ax3.plot(dfGlobalTem.index, dfGlobalTem['position'])


ani = animation.FuncAnimation(fig, animate, interval=300)
plt.show()
