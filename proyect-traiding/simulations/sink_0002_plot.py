# External libs
import sys
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
import matplotlib.animation as animation
from matplotlib import style
import numpy as np
from rx import Observable, Observer
from time import sleep
import random
import pandas as pd

class SingPlot(object):
    __instance = None

    __config = None
    __sink = None

    def __init__(self):
        if SingPlot.__instance != None:
            raise Exception("Esta clase no es singleton!")
        else:
            SingPlot.__instance = self

    @staticmethod
    def getInstance():
        if SingPlot.__instance == None:
            SingPlot()
        return SingPlot.__instance 
    

    def __priceBuffUpdate(self, array):
        print(array)


    def setFlow(self, config, flow):
        flow \
        .buffer_with_count(5, 1) \
        .subscribe(lambda array: self.__priceBuffUpdate(array))

    def plotFlow(self):
        
        fig = plt.figure()

        ax1 = plt.subplot2grid((3, 3), (0, 0), colspan=2)
        ax2 = plt.subplot2grid((3, 3), (1, 0), colspan=2, sharex=ax1, sharey=ax1)
        ax3 = plt.subplot2grid((3, 3), (2, 0), colspan=2, sharex=ax1)
        ax4 = plt.subplot2grid((3, 3), (0, 2), colspan=1)
        ax5 = plt.subplot2grid((3, 3), (1, 2), colspan=1)
        ax6 = plt.subplot2grid((3, 3), (2, 2), colspan=1)
        
        def animate(i):
            ax1.clear()
            ax2.clear()
            ax3.clear()
            ax4.clear()
            ax5.clear()
            ax6.clear()

            ax1.plot(range(10), range(10))

        anm = animation.FuncAnimation(fig, animate, interval=300)
        plt.show()
        print('**************')
        
