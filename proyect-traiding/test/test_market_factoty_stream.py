import sys
sys.path.append('../market-factory')
sys.path.append('../shared')

from market_factory import MarketFactory

market = MarketFactory.getInstance({})

market.GET_STREAM({}).subscribe(lambda s: print(s))
input("Press any key to quit\n")


