
import sys
sys.path.append('../market-factory')

from market_production import MarketProduction
from rx import Observable, Observer
import json

class PrintObserver(Observer):

    def on_next(self, value):
        print("Received {0}".format(json.dumps(value, indent=2)))

    def on_completed(self):
        print("Done!")

    def on_error(self, error):
        print("Error Occurred: {0}".format(error))

market = MarketProduction.getInstance({})
market.GET_STREAM({}).subscribe(PrintObserver())
input("Press any key to quit\n")