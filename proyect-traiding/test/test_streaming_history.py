import sys
sys.path.append('../algorithms')
sys.path.append('../shared')
sys.path.append('../simulations')
sys.path.append('../market-factory')

from al_0002_2mean import Al_0002_2Mean
from market_factory import MarketFactory

market = MarketFactory.getInstance({})

def func(s):
    print(s)

market.GET_STREAM({}) \
.subscribe(lambda s: func(s))
input("Press any key to quit\n")
