import sys
sys.path.append('../market-factory')
sys.path.append('../shared')

from market_factory import MarketFactory

market = MarketFactory.getInstance({})

market.SELL({})
market.BUY({})

print('Final')