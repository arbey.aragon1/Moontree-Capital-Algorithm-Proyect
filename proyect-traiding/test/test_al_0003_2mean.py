import sys
sys.path.append('../algorithms')
sys.path.append('../shared')
sys.path.append('../market-factory')

from al_0003_2mean import Al_0003_2Mean
from market_factory import MarketFactory

market = MarketFactory.getInstance({})

configDB = {
    'fileDataName':'data.csv',
    'fileDataNameOut':'dataOut.csv',
    'fileOrderName':'orders.csv'
    }
process = Al_0003_2Mean({'interval': 15},
    market.GET_STREAM({}),
    configDB)

newFlow = process.flowConnect()

def func(s):
    print(s)

newFlow.subscribe(lambda s: func(s))
input("Press any key to quit\n")