import sys
sys.path.append('../algorithms')
sys.path.append('../shared')
sys.path.append('../market-factory')

from al_0001_2mean import Al_0001_2Mean
from market_factory import MarketFactory

market = MarketFactory.getInstance({})

process = Al_0001_2Mean(
    {'interval': 15},
    market.GET_STREAM({}))

newFlow = process.flowConnect()

def func(s):
    print(s)

newFlow.subscribe(lambda s: func(s))
input("Press any key to quit\n")