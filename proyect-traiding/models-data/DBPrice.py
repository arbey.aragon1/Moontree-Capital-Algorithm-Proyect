# -*- coding: utf-8 -*-
__author__ = 'Arbey Aragon'

class DBPrice:

    def __init__(self, id, bid, ask, timeStamp, timeStamp2):
        self.id = id
        self.bid = bid
        self.ask = ask
        self.timeStamp = timeStamp
        self.timeStamp2 = timeStamp2

    def toDBCollection (self):
        return {
            "id":self.id,
            "bid":self.bid,
            "ask": self.ask,
            "timeStamp":self.timeStamp,
            "timeStamp2":self.timeStamp2
        }

    def __str__(self):
        return "id: %s ,  bid: %s ,  ask: %s ,  timeStamp: %s ,  timeStamp2: %s " \
               %(self.id, self.bid, self.ask, self.timeStamp, self.timeStamp2)