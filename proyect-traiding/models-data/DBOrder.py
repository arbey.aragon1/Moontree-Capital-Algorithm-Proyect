# -*- coding: utf-8 -*-
__author__ = 'Arbey Aragon'

class DBOrder:

    def __init__(self, id, price, position, timeStamp):
        self.id = id
        self.price = price
        self.position = position
        self.timeStamp = timeStamp

    def toDBCollection (self):
        return {
            "id":self.id,
            "price":self.price,
            "position": self.position,
            "timeStamp":self.timeStamp
        }

    def __str__(self):
        return "id: %s ,  price: %s ,  position: %s ,  timeStamp: %s " \
               %(self.id, self.price, self.position, self.timeStamp)