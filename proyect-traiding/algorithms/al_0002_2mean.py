import sys
sys.path.append('../algorithms-interface')
sys.path.append('../shared')

from algorithm_interface import AlgorithmInterface
from constants import LONG, SHORT, ANY

import numpy as np
import pandas as pd

class Al_0002_2Mean(AlgorithmInterface):
    STATUS_INIT=0
    STATUS_BUY=1
    STATUS_SELL=2

    __status = 0 
    __means = [44,165]
    
    def __meansCalc(self, array):
        df = pd.DataFrame(array)
        obj = array[-1]
        obj['m1'] = df['price'].iloc[-self.__means[0]:].mean()
        obj['m2'] = df['price'].iloc[-self.__means[1]:].mean()
        return obj
    
    def __difMeans(self, obj):
        obj['m1_m2'] = np.sign(obj['m1']-obj['m2'])
        return obj

    def __positionCalc(self, obj):
        configOrder = {}
        if(self.__status == self.STATUS_INIT):
            if(obj['m1_m2'] == 1):
                self.__status = self.STATUS_BUY
                configOrder['order'] = self.BUY
                self.sendOrder(configOrder)
            elif(obj['m1_m2'] == 0):
                pass
            elif(obj['m1_m2'] == -1):
                self.__status = self.STATUS_SELL
                configOrder['order'] = self.SELL
                self.sendOrder(configOrder)
                

        elif(self.__status == self.STATUS_BUY):
            if(obj['m1_m2'] == 1):
                pass
            elif(obj['m1_m2'] == 0):
                pass
            elif(obj['m1_m2'] == -1):
                self.__status = self.STATUS_INIT
                configOrder['order'] = self.SELL
                self.sendOrder(configOrder)
                

        elif(self.__status == self.STATUS_SELL):
            if(obj['m1_m2'] == 1):
                self.__status = self.STATUS_INIT
                configOrder['order'] = self.BUY
                self.sendOrder(configOrder)
            elif(obj['m1_m2'] == 0):
                pass
            elif(obj['m1_m2'] == -1):
                pass

        return obj

    def algoTraiding(self, sink):
        return sink \
        .buffer_with_count(max(self.__means), 1) \
        .map(lambda array: self.__meansCalc(array)) \
        .map(lambda t: self.__difMeans(t)) \
        .map(lambda y: self.__positionCalc(y)) \


        
        
        

