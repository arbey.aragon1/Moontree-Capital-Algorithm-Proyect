import sys
sys.path.append('../algorithms-interface')
sys.path.append('../shared')

from algorithm_interface import AlgorithmInterface
from constants import ANY, LONG, SHORT

import numpy as np
import pandas as pd

class Al_0001_2Mean(AlgorithmInterface):
    __means = [45,70]
    __oldValue = None
    __changePosition = False

    def __meansCalc(self, array):
        df = pd.DataFrame(array)
        obj = array[-1]
        obj['m1'] = df['price'].iloc[-self.__means[0]:].mean()
        obj['m2'] = df['price'].iloc[-self.__means[1]:].mean()
        return obj
    
    def __difMeans(self, obj):
        obj['m1_m2'] = np.sign(obj['m1']-obj['m2'])
        return obj

    def __positionCalc(self, obj):
        
        obj['position'] = ANY  # Ninguna
        obj['positionChanged'] = False

        if (self.__oldValue is not None):
            if(self.__oldValue['m1_m2'] != obj['m1_m2']):
                obj['positionChanged'] = True
                self.__changePosition = not(self.__changePosition)
                obj['position'] = [ANY, LONG][self.__changePosition]

        if((obj['m1_m2'] == 1) or (obj['m1_m2'] == -1)):
            self.__oldValue = obj
        
        return obj

    def algoTraiding(self, sink):
        return sink \
        .buffer_with_count(max(self.__means), 1) \
        .map(lambda array: self.__meansCalc(array)) \
        .map(lambda t: self.__difMeans(t)) \
        .map(lambda y: self.__positionCalc(y)) \


        
        
        

