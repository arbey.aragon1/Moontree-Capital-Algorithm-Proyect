# External libs
import sys
import numpy as np
from rx import Observable, Observer
from time import sleep
import random
import pandas as pd
import constants
import returnCalc as rC
from pymongo import MongoClient

# Paths
sys.path.append('./models-data')

# Proyect libs
from DBOrder import DBOrder
from DBPrice import DBPrice


class dao_service:
    def __init__(self):
        self._connectCreate = False

    def connectMongo(self):
        mongoClient = MongoClient('localhost', 27017)
        db = mongoClient['MTC']

        self._collectionVars = db['vars']
        self._collectionPrices = db['prices']
        self._collectionOrders = db['orders']

        emp = self._collectionVars.find_one(
            {
                "name": "idGeneratorVar"
            }
        )

        self._collectionVars.update_one(
            {
                "name": "idGeneratorVar"
            },
            {
                "$set":
                {
                    "val": emp['val']+1
                }
            }
        )
        self._idVal = emp['val']
        print(self._idVal)
        print()
        self._connectCreate = True

    def orderGenerate(self, order):
        testOrder = DBOrder(
            self._idVal,
            order['price'],
            order['position'],
            order['time']
        )
        self._collectionOrders.insert(testOrder.toDBCollection())

    def savePrice(self, price):
        testPrice = DBPrice(
            self._idVal,
            price['bid'],
            price['ask'],
            price['time'],
            price['time2']
        )
        self._collectionPrices.insert(testPrice.toDBCollection())
