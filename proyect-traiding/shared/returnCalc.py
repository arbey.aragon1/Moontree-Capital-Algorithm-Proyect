# External libs
from rx import Observable, Observer
import pandas as pd

# Proyect libs
import history_data_service as serviceData


def returnCal(obs):
    return obs \
        .map(lambda s: float(s)) \
        .start_with({
            'buffer': 0,
            'actual': None,
            'valid': False
        }) \
        .scan(lambda x, y: {
            'buffer': x['buffer']+y,
            'actual': y,
            'valid': True
        }) \
        .filter(lambda s: s['valid']) \
        .map(lambda s: {
            'returnList': s['actual'],
            'accumulatedList': s['buffer']
        }) \



def returnAcCal(obs):
    def retActTem(x, y):
        x.extend(y)
        return x
    return obs \
        .map(lambda x: [x]) \
        .scan(lambda x, y: retActTem(x, y)) \



def retAcCal(obs):
    return returnAcCal(returnCal(obs)) \
        .map(lambda s: pd.DataFrame(s)) \



#retAcCal(ff.randomWalk(mlSeconds = 100, initVal = 50, mu = 1, sigma = 0.5)) \
# .do_action(lambda s: print()) \
# .subscribe(lambda s: print(s))
#input("Press any key to quit\n")
